import java.time.LocalDate;

public class Requires {
    public static class Str{
        public static void NotNullOrEmpty (String value, String name){
            if(value == null || value.isEmpty()){
                throw new IllegalArgumentException(name + " cannot be null or empty");
            }
        }
    }

    public static class DateTime{
        public static void NotFuture(LocalDate dateTime, String argumentName) {
            boolean isFutureDateTime = dateTime.isAfter(LocalDate.now());
            if(isFutureDateTime) {
                throw new FutureBirthdayException(dateTime, argumentName);
            }
        }
    }

}
