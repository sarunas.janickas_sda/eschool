import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {

        Person michael = new Person("Michael", "Jackson", LocalDate.of(2019,9,18), 80f, 1.92f, Gender.MALE);
        Person vardenis = new Person("Vardenis", "Pavardenis", LocalDate.of(2002, 8, 1), 56.1f,1.80f, Gender.MALE);
        Person ana = new Person("Ana", "Karenina", LocalDate.of(1970, 2, 25), 50f,1.55f, Gender.FEMALE);

        Person[] people = {michael, vardenis, ana};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(System.out::println);

//        Person[] adultsArray = adultsStream.toArray(Person[]::new);



    }
}
